
# Cahn-Hilliard Equation using Mixed Models

This repository holds the python codes for running the experiments for solving the Cahn-Hilliard equation with a variable mobility using a mixed model approach. The numerical experiments verify this approach by testing convergence via a reference solution. 

## Dependencies

The following python libraries are used

```
numpy
matplotlib
```

## Computing a reference solution

The `constant` and `variable` directories contain an `rk4.py` that is used to compute the reference solution. The reference solution code is computed with sufficient number of time steps such that the solution is stable and would be considered the solution according to double precision. The reference solution can be generated for example with

```
cd constant
mkdir ref_sols
python rk4.py 64 5000000 1
```

Which would mean compute the reference solution for 64 fourier modes, 5000000 time steps, and 1 for save the solution. A reference solution should be generated for all fourier modes that wish to be tested against for convergence. Minimal error checking is done, and the code assumes that the `ref_sols` directory exists.

A similar procedure can be used for the `variable` codes. Once this is created the `variable/ref_sols` can be copied into `mixed` because they share the same final solution.

## Running the Experiment

Each directory has similar scripts to be used for each method to be tested. The experiments can then be run with (for example)

```
cd mixed
python semi_imr.py 64 10000
#or for corrections
python corr_semi_imr.py 64 10000 2
```

## Automated Experiments

Each directory has bash scripts that attempt to automate running the experiments for different ranges of dt and N. The RK4 data generation (ie run the convergence test of the rk4 against the reference solution) can be run simply with

```
bash rk4_experiment.sh
```

Other methods can then be run by providing the `general_experiment.sh` with the extensionless name of the experiment to be run. Examples:

```
bash general_experiment.sh semi_imr
bash general_experiment.sh semi_imp_euler
```

Output is collected in an `outputs` directory for each run of the experiments. The data is then accumulated in `outputs/<experiment_name>/n-<N>/{fullerrors,fulltimes}.txt`.

## Disclaimer

There are likely bugs everywhere. This project was a whirlwind thrown together in a few weeks so things could very much be done better. Use at your own risk.

## References

This repository also contains pdfs of the papers used to produce these experiments. Please refer to them for more information on the problems we are solving.