
import numpy as np
import matplotlib.pyplot as plt
import pyfftw.interfaces.numpy_fft as fft
import sys
import math

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def fi(phi):
    return -k2 * fft.fft2(-phi + phi**3)

plt.ion()
fig, ax = plt.subplots()
im = ax.imshow(phi, cmap='gray', extent=[0, L, 0, L], origin='lower')
plt.colorbar(im)

plot_every = nsteps // 1000

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-IMR
    y1_hat = (phi_hat + dt / 2 * (-k2 * fft.fft2(-phi + phi**3))) / (1 + dt / 2 * kappa * k4)
    y1 = fft.ifft2(y1_hat).real
    phi_hat_new = phi_hat + dt * (-k2 * fft.fft2(-y1 + y1**3) - kappa * k4 * y1_hat)

    #Update
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

    #plot the data
    if i % plot_every == 0:
        im.set_array(phi)
        plt.title(f"Time: {ttotal:.3f}")
        plt.draw()
        plt.pause(0.001)

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")

plt.ioff()
plt.show()