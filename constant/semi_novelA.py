
import numpy as np
import matplotlib.pyplot as plt
import pyfftw.interfaces.numpy_fft as fft
import sys
import math

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
A = np.zeros((4, 4))
A[1, 0] = 0.211324865405187
A[2, 0] = 0.709495523817170
A[2, 1] = -0.865314250619423
A[3, 0] = 0.705123240545107
A[3, 1] = 0.943370088535775
A[3, 2] = -0.859818194486069

Ae = np.zeros((4, 4))
Ae[0, 0] = 0.788675134594813
Ae[2, 0] = 0.051944240459852
Ae[2, 2] = 0.788675134594813

B = np.zeros(4)
B[1] = 0.5
B[3] = 0.5

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def fa(x):
    return -k2 * fft.fft2(-x + x**3)

def fi(x, x_hat):
    return -k2 * fft.fft2(-x + x**3) - kappa * k4 * x_hat

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-novelA
    y1_hat = (phi_hat + Ae[0, 0] * dt * fa(phi)) / (1.0 + Ae[0, 0] * dt * kappa * k4)
    y1 = fft.ifft2(y1_hat).real

    y2_hat = (phi_hat + A[1, 0] * dt * fi(y1, y1_hat))
    y2 = fft.ifft2(y2_hat).real

    y3_hat = (phi_hat + 
        dt * (A[2, 0] * fi(y1, y1_hat) + A[2, 1] * fi(y2, y2_hat)) + 
        dt * (Ae[2, 0] * fa(y1) + Ae[2, 2] * fa(phi))
        ) / (1 + Ae[2, 2] * dt * kappa * k4)
    y3 = fft.ifft2(y3_hat)

    y4_hat = phi_hat + dt * (A[3, 0] * fi(y1, y1_hat) + A[3, 1] * fi(y2, y2_hat) + A[3, 2] * fi(y3, y3_hat))

    #Update
    phi_hat_new = phi_hat + B[1] * dt * fi(y1, y1_hat) + B[3] * dt * fi(y2, y2_hat)
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")