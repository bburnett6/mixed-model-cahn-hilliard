import numpy as np
import numpy.fft as fft
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
import sys

# Define the parameters
L = 100 # domain length
N = 64 # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = 5000 # number of time steps
dt = (tf - ti) / nsteps

# Define the grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

#Objective function for implicit solve
def rhs(x, y, fdt):
        return y + fdt * (-k2 * fft.fft2(-(fft.ifft2(x).real) + (fft.ifft2(x).real)**3) - kappa * k4 * x) - x

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Implicit Euler step
    phi_hat_new = fsolve(rhs, phi_hat, args=(phi_hat, dt))

    #Update
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

fig, ax = plt.subplots()
ax.imshow(phi, cmap='gray')
plt.show()
