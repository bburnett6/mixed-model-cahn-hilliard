
import numpy as np
import matplotlib.pyplot as plt
import pyfftw.interfaces.numpy_fft as fft
import sys
import math

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
gamma = (math.sqrt(3.0) + 3.0) / 6.0 
ncorr = int(sys.argv[3])

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def fa(x):
    return -k2 * fft.fft2(-x + x**3)

def fi(x, x_hat):
    return -k2 * fft.fft2(-x + x**3) - kappa * k4 * x_hat

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-SDIRK
    y1_hat = (phi_hat + gamma * dt * fa(phi)) / (1.0 + gamma * dt * kappa * k4)
    y1 = fft.ifft2(y1_hat).real
    #Flat corrections
    """
    for c in range(ncorr):
        yk_hat = phi_hat + gamma * dt * fi(y1, y1_hat)
        yk = fft.ifft2(yk_hat)
        y1_hat = np.copy(yk_hat)
        y1 = np.copy(yk)
    """
    #Addaptive Corrections
    yk_hat = phi_hat + gamma * dt * fi(y1, y1_hat)
    yk = fft.ifft2(yk_hat)
    r0 = yk_hat - y1_hat
    for c in range(ncorr):
        yr_hat = phi_hat + gamma * dt * fi(yk, yk_hat)
        yr = fft.ifft2(yr_hat)
        r1 = yr - yk 
        if np.linalg.norm(r1) < np.linalg.norm(r0):
            y1 = yk 
            y1_hat = yk_hat
            yk = yr 
            yk_hat = yr_hat
            r0 = r1

    y2_hat = (phi_hat + (1.0 - 2.0*gamma) * dt * fi(y1, y1_hat) + gamma * dt * fa(phi)) / (1.0 + gamma * dt * kappa * k4)
    y2 = fft.ifft2(y2_hat).real
    #Flat corrections
    """
    for c in range(ncorr):
        yk_hat = phi_hat + (1.0 - 2.0*gamma) * dt * fi(y1, y1_hat) + gamma * dt * fi(y2, y2_hat)
        yk = fft.ifft2(yk_hat)
        y2_hat = np.copy(yk_hat)
        y2 = np.copy(yk)
    """
    #Addaptive Corrections
    yk_hat = phi_hat + (1.0 - 2.0*gamma) * dt * fi(y1, y1_hat) + gamma * dt * fi(y2, y2_hat)
    yk = fft.ifft2(yk_hat)
    r0 = yk_hat - y2_hat
    for c in range(ncorr):
        yr_hat = phi_hat + (1.0 - 2.0*gamma) * dt * fi(y1, y1_hat) + gamma * dt * fi(yk, yk_hat)
        yr = fft.ifft2(yr_hat)
        r1 = yr - yk 
        if np.linalg.norm(r1) < np.linalg.norm(r0):
            y2 = yk 
            y2_hat = yk_hat
            yk = yr 
            yk_hat = yr_hat
            r0 = r1
    #Update
    phi_hat_new = phi_hat + 0.5 * dt * (fi(y1, y1_hat) + fi(y2, y2_hat))

    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
