
import numpy as np
import matplotlib.pyplot as plt
import pyfftw.interfaces.numpy_fft as fft
import math
import sys

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
is_save = bool(int(sys.argv[3])) #Save the solution: 0 False else True

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

np.seterr(all='raise')

def fi(phi, phi_hat):
    return -k2 * fft.fft2(-phi + phi**3) - kappa * k4 * phi_hat

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    print(i, end='\r')
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #rk4
    r_k1 = fi(phi, phi_hat)

    tmp_hat = phi_hat + 0.5 * dt * r_k1
    tmp = fft.ifft2(tmp_hat).real
    r_k2 = fi(tmp, tmp_hat)

    tmp_hat = phi_hat + 0.5 * dt * r_k2
    tmp = fft.ifft2(tmp_hat).real
    r_k3 = fi(tmp, tmp_hat)

    tmp_hat = phi_hat + dt * r_k3
    tmp = fft.ifft2(tmp_hat).real
    r_k4 = fi(tmp, tmp_hat)

    #Update
    phi_hat_new = phi_hat + dt * (r_k1 + 2.0*r_k2 + 2.0*r_k3 + r_k4) / 6.0
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

#Save solution
fname = f"ref_sols/ref_sol_n{N}.npy"
if is_save:
    print(f"Saving solution to {fname}")
    with open(fname, 'wb') as f:
        np.save(f, phi)
else:
    with open(fname, 'rb') as f:
        ref_sol = np.load(f)

    err_tmp = phi - ref_sol
    err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
    print(f"L2 Error with reference: {err}")
