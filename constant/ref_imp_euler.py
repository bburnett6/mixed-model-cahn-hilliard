
import numpy as np
import matplotlib.pyplot as plt
#import pyfftw.interfaces.numpy_fft as fft
import numpy.fft as fft
import sys
import math
import time

# Define the parameters
L = 1024 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 30000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
is_save = bool(int(sys.argv[3])) #Save the solution: 0 False else True

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def fi(phi):
    return -k2 * fft.fft2(-phi + phi**3)

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
start = time.time()
for i in range(nsteps):
    print(i, end='\r')
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-Implicit
    phi_hat_new = (phi_hat + dt * fi(phi)) / (1.0 + dt * kappa * k4)

    #Update
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

elapsed = time.time() - start

#Save solution
fname = f"ref_sols/ref_imp_euler_n{N}.npy"
if is_save:
    print(f"Saving solution to {fname}")
    with open(fname, 'wb') as f:
        np.save(f, phi)
    print(f"Time to solution: {elapsed}")
else:
    with open(fname, 'rb') as f:
        ref_sol = np.load(f)

    err_tmp = phi - ref_sol
    err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
    print(f"L2 Error with reference: {err}")
    print(f"Elapsed time: {elapsed}")

"""
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(phi, cmap='gray')
ax2.imshow(ref_sol, cmap='gray')
plt.show()
"""