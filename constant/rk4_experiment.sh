#!/bin/bash

TS=(5000 10000 20000 40000 80000 160000)
NS=(64 128 256)
OUTPUTBASE=./outputs/rk4

for n in ${NS[@]}
do
	outputdir=$OUTPUTBASE/n-$n
	if [ ! -d "$outputdir" ]
	then
		mkdir -p $outputdir
	fi

	for nt in ${TS[@]}
	do
		echo "python rk4.py $n $nt 0"
		python rk4.py $n $nt 0 > $outputdir/nt-$nt.txt
	done

	#Post process everything into a single file
	#Explaination:
	#1. Grep for the time line
	#2. Sort the grep output "fname:linedata" by splitting on : (-t:) and getting column 1 (-k1) and sort it numerically (-V)
	#3. Print the last column ($NF) of the sorted output which contains the time
	grep "time" $outputdir/* | sort -V -t: -k1 | awk '{print $NF}' > $outputdir/fulltimes.txt
	grep "Error" $outputdir/* | sort -V -t: -k1 | awk '{print $NF}' > $outputdir/fullerrors.txt

done