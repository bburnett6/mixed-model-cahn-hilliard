
import numpy as np
import matplotlib.pyplot as plt
import pyfftw.interfaces.numpy_fft as fft
import sys
import math

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
ncorr = int(sys.argv[3])

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def fi(phi):
    return -k2 * fft.fft2(-phi + phi**3)

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-IMR
    y1_hat = (phi_hat + dt / 2 * (-k2 * fft.fft2(-phi + phi**3))) / (1 + dt / 2 * kappa * k4)
    y1 = fft.ifft2(y1_hat).real

    #corrections
    """
    #Flat corrections
    for c in range(ncorr):
        yk_hat = phi_hat + dt / 2 * (-k2 * fft.fft2(-y1 + y1**3) - kappa * k4 * y1_hat)
        yk = fft.ifft2(yk_hat)
        y1_hat = np.copy(yk_hat)
        y1 = np.copy(yk)
    """
    #Addaptive corrections
    
    yk_hat = phi_hat + dt / 2 * (-k2 * fft.fft2(-y1 + y1**3) - kappa * k4 * y1_hat)
    yk = fft.ifft2(yk_hat)
    r0 = yk_hat - y1_hat
    for c in range(ncorr):
        yr_hat = phi_hat + dt / 2 * (-k2 * fft.fft2(-yk + yk**3) - kappa * k4 * yk_hat)
        yr = fft.ifft2(yr_hat)
        r1 = yr - yk 
        """
        #interesting to look at this
        n1 = np.linalg.norm(r1, np.inf)
        n2 = np.linalg.norm(r0, np.inf)
        if n1 < n2 and n1 < 1.0 and n2 < 1.0:
            #print(i, np.linalg.norm(r1, np.inf), np.linalg.norm(r0, np.inf))
        """
        if np.linalg.norm(r1, np.inf) < np.linalg.norm(r0, np.inf):
            y1 = yk 
            y1_hat = yk_hat
            yk = yr 
            yk_hat = yr_hat
            r0 = r1
    

    #Update
    phi_hat_new = phi_hat + dt * (-k2 * fft.fft2(-y1 + y1**3) - kappa * k4 * y1_hat)
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")