
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
#import pyfftw.interfaces.numpy_fft as fft
import numpy.fft as fft
import sys
import math

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
ti = 0.0
tf = 1000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps

# Define the grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

def rhs(x, y, fdt):
    x = x.reshape(N, N)
    fx = fft.fft2(x)
    val = fft.fft2(y) + fdt * (-k2 * fft.fft2(-x + x**3) - kappa * k4 * fx) - fx
    return np.ravel(fft.ifft2(val).real)

# Solve the Cahn-Hilliard equation using spectral methods
ttotal = 0.0
for i in range(nsteps):
    #Ensure final time is reached
    print(i)
    if i == nsteps - 1:
        dt = tf - ttotal

    #Implicit Euler step
    phi_new = optim.fsolve(rhs, phi, args=(phi, dt)).reshape(N, N)

    #Update
    phi = phi_new
    ttotal += dt

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(phi, cmap='gray')
ax2.imshow(ref_sol, cmap='gray')
plt.show()