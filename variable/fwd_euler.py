
import numpy as np
import matplotlib.pyplot as plt
#import pyfftw.interfaces.numpy_fft as fft
import numpy.fft as fft
import sys
import math
import time

# Define the parameters
L = 1024 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
a = 0.5
ti = 0.0
tf = 30000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k1 = np.sqrt(kx**2 + ky**2)
k2 = kx**2 + ky**2
k4 = k2**2

if not np.all(dt * kappa * k4 < 1.0):
    print("Condition [[ dt * kappa * k4 < 1 ]] not met")
    sys.exit(0)

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

# Solve the Cahn-Hilliard equation using spectral methods
np.seterr(all='raise')
ttotal = 0.0
start = time.time()
A = 0.5 * (np.max(1.0 - a * phi**2) + np.min(1.0 - a * phi**2))
#A = 0.5
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Semi-Implicit
    phi_hat_new = phi_hat + dt * (
        1j * k1 * fft.fft2(
            (1.0 - a * phi**2) * fft.ifft2(
                1j * k1 * (fft.fft2(-phi + phi**3) + kappa * k2 * phi_hat)
            )
        )
    )

    #Update
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

elapsed = time.time() - start

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")

"""
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(phi, cmap='gray')
ax2.imshow(ref_sol, cmap='gray')
plt.show()
"""