
import numpy as np
import matplotlib.pyplot as plt
#import pyfftw.interfaces.numpy_fft as fft
import numpy.fft as fft
import sys
import math
import time 

# Define the parameters
L = 1024 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing
kappa = 1.0 # mobility
a = 0.5
ti = 0.0
tf = 30000.0 # total simulation time
nsteps = int(sys.argv[2]) # number of time steps
dt = (tf - ti) / nsteps
gamma = (3.0 + math.sqrt(3)) / 6.0
delta = -2.0 * math.sqrt(2.0) / 3.0

# Define the grid
x = np.linspace(0, L-dx, N)
k = np.fft.fftfreq(N, d=dx/(2*np.pi))
kx, ky = np.meshgrid(k, k)
k1 = np.sqrt(kx**2 + ky**2)
k2 = kx**2 + ky**2
k4 = k2**2

# Define the initial condition
np.random.seed(1234)
phi = np.random.normal(0, 0.1, (N,N))

plt.ion()
fig, ax = plt.subplots()
im = ax.imshow(phi, cmap='gray', extent=[0, L, 0, L], origin='lower')
plt.colorbar(im)

plot_every = nsteps // 1000

# Solve the Cahn-Hilliard equation using spectral methods
np.seterr(all='raise')
ttotal = 0.0
start = time.time()
A = 0.5 * (np.max(1.0 - a * phi**2) + np.min(1.0 - a * phi**2))
#A = 0.5
print(np.all(dt * kappa * k4 < 1.0))
for i in range(nsteps):
    #Ensure final time is reached
    if i == nsteps - 1:
        dt = tf - ttotal

    #Get fourier space solution
    phi_hat = fft.fft2(phi)
    #Stage 1
    y1_hat = phi_hat 
    y1 = fft.ifft2(y1_hat)

    #stage 2
    y2_hat = (phi_hat - gamma * dt * k2 * fft.fft2(-y1 + y1**3)) / (1.0 + gamma * dt * kappa * k4)
    y2 = fft.ifft2(y2_hat)

    #stage 3
    y3_hat = phi_hat - delta * dt * k2 * fft.fft2(-y1 + y1**3)
    y3_hat -= (1.0 - delta) * dt * k2 * fft.fft2(-y2 + y2**3)
    y3_hat -= (1.0 - gamma) * dt * kappa * k4 * y2_hat
    y3_hat = y3_hat / (1.0 + gamma * dt * kappa * k4)
    y3 = fft.ifft2(y3_hat)

    #Forward time step
    phi_hat_new = phi_hat + (1.0 - gamma) * dt * (
        1j * k1 * fft.fft2(
            (1.0 - a * y2**2) * fft.ifft2(
                1j * k1 * (fft.fft2(-y2 + y2**3) + kappa * k2 * y2_hat)
                )
            )
        )
    phi_hat_new += 0.5 * gamma * (
        1j * k1 * fft.fft2(
            (1.0 - a * y3**2) * fft.ifft2(
                1j * k1 * (fft.fft2(-y3 + y3**3) + kappa * k2 * y3_hat)
                )
            )
        )

    #Update
    phi = fft.ifft2(phi_hat_new).real
    ttotal += dt

    #plot the data
    if i % plot_every == 0:
        im.set_array(phi)
        plt.title(f"IMEX (2,3,3) Time: {ttotal:.3f}")
        plt.draw()
        plt.pause(0.001)

elapsed = time.time() - start

#Check error
fname = f"ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
    ref_sol = np.load(f)

err_tmp = phi - ref_sol
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")

plt.ioff()
plt.show()