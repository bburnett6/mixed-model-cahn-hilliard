
import numpy as np 
import matplotlib.pyplot as plt
import math
import sys

# Define the parameters
L = 100 # domain length
N = int(sys.argv[1]) # number of grid points
dx = L/N # grid spacing

#Check error
fname1 = f"constant/ref_sols/ref_sol_n{N}.npy"
with open(fname1, 'rb') as f:
    ref_sol1 = np.load(f)

fname2 = f"variable/ref_sols/ref_sol_n{N}.npy"
with open(fname2, 'rb') as f:
    ref_sol2 = np.load(f)

err_tmp = ref_sol1 - ref_sol2
err = math.sqrt(dx * np.tensordot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.imshow(ref_sol1, cmap='gray')
ax1.title.set_text('Constant Mobility RK4')
ax2.imshow(ref_sol2, cmap='gray')
ax2.title.set_text('Variable Mobility RK4')
plt.show()